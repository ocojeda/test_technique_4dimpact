import requests

def http_request(user_agent=""):
    URL = "https://httpbin.org/anything"
    PARAMS = {'msg': "welcome new user", "isadmin" : "1"}
    user_handler= ""

    if isinstance(user_agent , str):
        if user_agent == "":
            return requests.post(url = URL, params = PARAMS)
        return requests.post(url = URL, params = PARAMS, headers={'User-Agent': user_agent})
    else:
        return requests.post(url = URL, params = PARAMS)
    
print(http_request("toto").json())
print(http_request(12345).json())
print(http_request("").json())
print(http_request().json())

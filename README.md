Test in python 3.5.7 for dataimpact technical test

to install:

Copy the repository from gitlab 
git clone https://gitlab.com/ocojeda/test_technique_4dimpact.git

to git checkout to branch test_octavio:
git checkout test_octavio

install venv un python3: 
	python3 -m venv py3venv

launch vm
	source py3venv/bin/activate

install required modules
	pip install -r REQUIREMENTS.txt
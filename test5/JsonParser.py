import json, csv
import os, logging

logging.basicConfig(filename='info.log', filemode='w', level=logging.INFO)
employ_data = open('available products.csv', 'w')
csvwriter = csv.writer(employ_data)
count = 0

class jsonParser():
    data = ""
    json_file = ""
    def __init__(self):
        with open('../data.json') as self.json_file:
            self.data = json.load(self.json_file)
    
    def dispatch(self, numero):
        count = 0
        for items in self.data["Bundles"]:
            if count == 0 and numero == 3:
                header = items.keys()
                csvwriter.writerow(header)
                count += 1

            name = items.get("Name")
            products = items.get("Products")
            product = items.get("Product")

            if name and products:
                pr = products[0].get("Stockcode")
                price = products[0].get("Price")
                stock = products[0].get("IsInStock")

                if price == None:
                    price = 0
                if (numero == 1): print("You can buy "+ name + "===>" + "https://www.woolworths.com.au/shop/productdetails/" + str(pr) + " at this price : " + str(price))
                if stock == False:
                    if numero == 2: logging.info(name+"unavailable")
                else:
                    if numero == 3: csvwriter.writerow(items.values())

            elif name and product:
                pr = product[0].get("Stockcode")
                price = product[0].get("Price")
                stock = product[0].get("IsInStock")
                if stock == False:
                    if numero == 2: logging.info(name+"unavailable")
                else:
                    if numero == 3: csvwriter.writerow(items.values())
                if price == None:
                    price = 0

                if (numero == 1): print("You can buy "+ name + "===>" + "https://www.woolworths.com.au/shop/productdetails/" + str(pr) + " at this price : " + str(price))
            elif name:
                if numero == 2: logging.error(name+"unavailable")
                if (numero == 1) : print("You can buy " + name + " nowhere")

    def __del__(self):
        self.json_file.close()
    def Printme(self):
        self.dispatch(1)
    def LogUnavailableProds(self):
        self.dispatch(2)
    def SaveToCsv(self):
        self.dispatch(3)



test = jsonParser()
test.Printme()
test.LogUnavailableProds()
test.SaveToCsv()
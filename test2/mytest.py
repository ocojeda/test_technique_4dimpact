import unittest
from functional import *
 
class TestFunctional(unittest.TestCase):
    def test_get_element(self):
        simple_comme_bonjour = (["4", "3", "-2"])
        element = calculate(simple_comme_bonjour)
        self.assertEqual(element, 5)
    def test_get_element0(self):
        self.assertEqual(calculate(453), False)
    def test_get_element1(self):
        self.assertEqual(calculate(["nothing", 3, "8", 2, "1"]), 9)
    def test_get_element2(self):
        self.assertEqual(calculate(["nothing", 3, "8", 2.5, "1"]), 9)
    def test_get_element3(self):
        self.assertEqual(calculate(["nothing", 3, "8.5", 2.5, "1"]), 1)
 
if __name__ == '__main__':
    unittest.main()
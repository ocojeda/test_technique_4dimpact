# coding: utf-8
def calculate(mylist):
    total = 0
    if isinstance(mylist, list):
        for elems in mylist:
            if type(elems) is str:
                if elems.lstrip("-").isnumeric():
                    total += int(elems)
        return total
    return total

# coding: utf-8
def calculate(mylist):
    total = 0
    if isinstance(mylist, list):
        for elems in mylist:
            if type(elems) is str:
                if elems.lstrip("-").isnumeric():
                    total += int(elems)
        return total
    return total

def multiplyList(myList) :
    result = 1
    for x in myList: 
         result = result * int(x)  
    return result

def persistence(num):
    if isinstance(num, int):
        tmp = str(num).lstrip("-")
        total = int(tmp)
        count = 0
        if total >= 1 and total <= 9:
            return 0
        while total > 9:
            total = multiplyList(tmp)
            tmp = str(total)
            count += 1
            if tmp.find("0") != -1:
                return count
        return count
    return 0

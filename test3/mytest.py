import unittest
from functional import *
 
class TestFunctional(unittest.TestCase):
    def test_get_element(self):
        self.assertEqual(persistence(1), 0)
    def test_get_element1(self):
        self.assertEqual(persistence([0,0,0]), 0)
    def test_get_element2(self):
        mytest =  ['c', 'f', 'm']
        self.assertEqual(persistence(mytest), 0)
    def test_get_element3(self):
        mytest =  "54"
        self.assertEqual(persistence(mytest), 0)
    def test_get_element4(self):
        self.assertEqual(persistence(999), 4)
    def test_get_element5(self):
        self.assertEqual(persistence(9909), 1)
    def test_get_element6(self):
        self.assertEqual(persistence(52), 1)
    def test_get_element7(self):
        self.assertEqual(persistence(8), 0)
    def test_get_element8(self):
        self.assertEqual(persistence(-55), 2)
 
if __name__ == '__main__':
    unittest.main()